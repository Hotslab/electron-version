# CHANGELOG
All notable changes to this project will be documented in this file.


## [1.0.4] - 2020-03-07
### Description
Features for clearing and deletion items

### Added 
- Deletion media item in opened view
- Clearing selected media items (without deletion from disc)
- Changelog



## [1.0.3] - 2020-03-03
### Description
The application learned to check for a new version

### Added 
- Updater - module for check a new version

### Changed
- Reconfigure CI/CD



## [1.0.2] - 2020-01-20
### Description
Small changes. Basically repository settings and CI/CD configurations

### Added 
- License (MIT)

### Changed
- New app icon



## [1.0.1] - 2019-10-17
### Description
VR player birthday  🎉 
### Added 
- View directory as a gallery of media files
- Support VR photo (.jpg)
- Support VR video (.mp4 – up to 4K – limited capacity by Chrome)
- Easy to delete unnecessary media
- Capability to clear accumulated cache
